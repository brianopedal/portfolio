import { courses } from "./data/courses.js";

export function establishTableHeaders(table, data) {
  const thead = table.createTHead();
  const row = thead.insertRow();

  data.map((headings) => {
    if (headings !== "id") {
      const th = document.createElement("th");
      const txt = document.createTextNode(headings);
      th.append(txt);
      row.append(th);
    }
  });
}

export function establishRows(table, data) {
  for (let element of data) {
    const row = table.insertRow();

    for (let key in element) {
      if (key !== "id") {
        const cell = row.insertCell();
        const txt = document.createTextNode(element[key]);
        cell.appendChild(txt);
      }
    }
  }
}

let table = document.querySelector("table");
let data = Object.keys(courses[0]);
establishTableHeaders(table, data);
establishRows(table, courses);
