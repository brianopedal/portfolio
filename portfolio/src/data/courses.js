export const courses = [
    { id: 1, title: "Getting Started with CSS", teacher: "Jen Kramer", company: "Frontend Masters", completionDate: new Date(2022, 5, 11), summary: "Get to know CSS in the right way without the fluff. You'll style text, build navigation bars, headers/footers, add icons, and more. You'll even build a professional portfolio website by the end of the course!" },
    { id: 2, title: "HTML and CSS: Creating a Basic Website", teacher: "Jon Friskics", company: "Pluralsight", completionDate: new Date(2020, 6, 6), summary: "Learn the fundamentals of HTML and CSS, while building a strong foundation for more advanced front-end development. This interactive course was formerly known as Front-end Foundations on Code School." },
];

export default courses;