

# Resume for Brian Opedal

## Summary
    - Focus: readable, well tested code
    - Experience (time, and subjects): 4 years c++ specification implementation, unit and blackbox testing, analysis and debugging
    - Strongest suits: determined, positive, reliable, collaborative

## Highlights
- Test Driven Development
- Software Design
- Business Logic programming in C++
- Unit and blackbox testing
- Debugging and analysis

## Experience
- **Project Engineer: Software Developer - Current** (2014-Current)
    - *[Kongsberg Defence & Aerospace, Kongsberg, Norway](https://www.kongsberg.com/en/kds/)*

## Education
- **Bachelor of Software Engineering, Embedded Systems & [Awarded Best Bachelor Project](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/best_bachelor_project.pdf)** (2014)
    - [*University of South-Eastern Norway, Kongsberg, Norway*](https://www.usn.no/studier/finn-studier/ingenior-sivilingenior-teknologi-og-it/bachelor-i-ingeniorfag-dataingenior/)
- **Bachelor of Circus Arts - Specialization Juggling and Movement** (2006)
    - *[Swinburne University of Technology, National Institute of Circus Arts, Melbourne, Australia](https://www.swinburne.edu.au/study/course/international/bachelor-of-circus-arts/)*


## Courses

- **[Agile Object-Oriented Design](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/agile_oop_design.pdf)** (2016)

- **[Test Driven Development](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/tdd_certificate_programmutvikling.pdf)** (2016)

- **[Advanced C++ and Intro to C++11](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/advanced_cpp_and_intro_to_cpp11.pdf)** (2015)


## Projects

- **draupnir - Cryptocurrency project currently in hiatus** (2018)

- **[Video Coacher](https://brage.bibsys.no/xmlui/handle/11250/196306) - Open-source video training enhancement system.** (2014)
