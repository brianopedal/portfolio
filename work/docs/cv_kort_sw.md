

# Brian Opedal
Leil. 315, Wergelands vei 23, 3613 Kongsberg | brianopedal@mac.com | 906 69 578 | født: 03.05.79

## WIP: Nøkkelkvalifikasjoner/sammendrag

## Arbeidserfaring
- **Prosjekt Ingeniør & Software utvikler**  (2014 - Nåværende)
    - [Kongsberg Defence & Aerospace - Missile Division](https://www.kongsberg.com/en/kds/)

- **Prosjektveileder** - Localhawk StudentProsjekt (2017) 


## Utdannelse

- [**Bachelor of Software Engineering – Embedded Systems**](https://www.usn.no/studier/finn-studier/ingenior-sivilingenior-teknologi-og-it/bachelor-i-ingeniorfag-dataingenior/) (2014) - [Universitetet i Sørøst-Norge](https://www.usn.no/)


## Prosjekter
- **[draupnir]()** - Cryptocurrency Token - Currently on hold (2018)
    - Medgrunnlegger
- **[Video Coacher](https://brage.bibsys.no/xmlui/handle/11250/196306)** - [Awarded Best Bachelor Project](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/best_bachelor_project.pdf) - Open-Source video training enhancement system - (2014)
    - Prosjektleder 


## Kurs
- [DEV279x: Building Interactive Prototypes using JavaScript](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/Microsoft_DEV279x_Certificate___edX.pdf) (19.12.2018)
- [Intro to JavaScript - Fundamentals of the JavaScript Syntax](https://www.udacity.com/course/intro-to-javascript--ud803) (24.06.2018)
- [Intro to HTML and CSS - Build styled, well-structured websites](https://www.udacity.com/course/intro-to-html-and-css--ud001) (26.06.2018)
- [Networking for Web Developers - TCP, IP, What's Underneath HTTP?](https://www.udacity.com/course/networking-for-web-developers--ud256) (27.06.2018)
- [Version Control with Git - Learn to track and manage changes](https://www.udacity.com/course/version-control-with-git--ud123) (28.06.2018)
- [Agile Object-Oriented Design](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/agile_oop_design.pdf) (04.11.2016)
- [Test Driven Development](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/tdd_certificate_programmutvikling.pdf) (22.04.2016)
- [Advanced C++ and Intro to C++11](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/advanced_cpp_and_intro_to_cpp11.pdf) (17.09.2015)


# IT-Kunnskaper
- **Versionskontroll:** clearcase, git
- **Kodereview:** Code Collaborator
- **OS:** Linux, OSX, Windows
- **Sw Principles:** TDD, SOLID, Patterns 
- **IDE**: PyCharm, Spyder, Vim, QtCreator, Xcode, Atom


# Fritidsinteresser
- sjonglering
- programmering
- fantasy/sci-fi
- gaming

# Referanser, vitnemål og attester
Ved forespørsel
