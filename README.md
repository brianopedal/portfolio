# Brian Opedal - Curriculum Vitae - Overview


[linkedin](./linkedin.md)

**Simple overview over education and courses I've taken, books I've read and projects I've done.**

*I did a complete career change to software engineering, so much of my previous education is related to performing arts. While this shines through to some degree, the focus here is on computer engineering and data science.*


## Education

[Bachelor in Engineering – Software Engineer - Embedded Systems](https://www.usn.no/studier/finn-studier/ingenior-sivilingenior-teknologi-og-it/bachelor-i-ingeniorfag-dataingenior/) & [Awarded Best Bachelor Project](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/best_bachelor_project.pdf) (2014)

[Bachelor of Circus Arts - Specialization Juggling and Movement](https://www.swinburne.edu.au/study/course/international/bachelor-of-circus-arts/) (2006)


## Projects:

### draupnir - cryptocurrency token project

### [Video Coacher](https://openarchive.usn.no/usn-xmlui/handle/11250/196306) - Open-Source video training enhancement system.


## Completed Courses


#### 2022

### [Frontend Masters](https://frontendmasters.com)

**Intermediate React, v4** by Brian Holt (2022-05-26) - *(Duration: 7 hours, 42 minutes)*
- Learn to build scalable React applications using the latest tools and techniques available in the React ecosystem! This course is modular, where you can pick and choose various react ecosystems you want to learn. You’ll learn hooks in-depth, CSS-in-JS with TailwindCSS, increase performance with code splitting and server-side rendering, add TypeScript, state management with Redux, test your app with Jest …and more!

**Complete Intro to React, v7** by Brian Holt (2022-05-23) - *(Duration: 7 hours, 38 minutes)*
- Much more than an intro, you’ll build with the latest features in React, including hooks, effects, context, and portals. Learn to build real-world apps from the ground up using the latest tools in the React ecosystem, like Parcel, ESLint, Prettier, and React Router!


**Rx.js Fundamentals** by  Steve Kinney (17.05.2022)
- Rx.js is a really useful JavaScript library for managing events that happen over time. You'll learn the foundation of Rx.js by creating an observable, which is the fundamental building block of Rx.js, from scratch. Then learn to manipulate data that arrives over time with operators. And build up to orchestrating multiple API requests to craft complex asynchronous experiences. By the end of the course, you'll be able to solve common UX patterns that would be otherwise tricky in vanilla JavaScript!

### Pluralsight

**Managing Container Images** by David Clinton (2022-04-20)
- Looking to effectively and securely work with container images? This course will introduce you to the Docker and Kubernetes tools you'll need, including how to host your own private image repository using the open source Docker Registry.

**Docker Deep Dive** by Nigel Poulton (2022-03-16)
- Containers are taking the world by storm. It teaches you everything you need to know to get started in the world of Docker and containers. It also provides a solid foundation for learning Kubernetes, and taking the Docker Certified Associate exam.

**


#### 2021

**Codeacademy: Learn React**
- Approx 10-20 hours of react material


### [Pluralsight Courses](https://learn.pluralsight.com)

**Building and Running Custom ASP.NET Core Containers** by Dan Wahlin (2021-12-04)
- If you're interested in running ASP.NET Core applications, RESTful services, or microservices in containers, we want to invite you to view Dan Wahlin's webinar.



**Client Side React Router 4** by David Starr (2021-12-02)
- React Router 4 offers a new dynamic paradigm for routing in React.js applications. This course shows the strength of dynamic routing and the possibilities of changing how you write React.js code when a Route becomes a first-class React component.

**Developing Python Apps with Docker** by Steven Haines (2021-12-02)
- Moving from a traditional architecture to a containerized architecture can be intimidating. This course will teach you how to use Docker and Docker Compose to build and deploy highly scalable Python applications.

**Securing the Docker Platform** by by Nigel Brown (2021-12-01)
- The Docker platform is a key ingredient in the packaging and delivery of container-based application services. This course will give you all the knowledge you need to enable you to securely manage the operation of a Docker platform.

**Docker for Web Developers** by Dan Wahlin (2021-11-27)
- Docker can bring many benefits to your development workflow and deployment process. You'll learn how to use Docker tools and commands, how to work with images and containers, container orchestration techniques, and much more.

**Building and Running Your First Docker App** by Dan Wahlin (2021-11-23)
- This course will teach you the core fundamentals needed to build and run an application using Docker containers.


**Using React 17 Hooks** (2021-11-15) by Peter Kellner
- React Hooks bring state and lifecycle events to React Functional Components as well as streamlining code that previously was coupled and complex. Learn techniques for using React Hooks including Redux-like state management with React Context.

**Webinar: Getting Started with Docker Containers** by Dan Wahlin (2021-11-05)
- Join expert Dan Wahlin as he breaks down what you need to know to get started and be successful with Docker.

**Fundamentals of Functional Programming in JavaScript** by by Nate Taylor (2021-10-16)
- Functional programming is gaining traction in the industry. This course will teach you the foundations of functional programming using JavaScript in easy to understand terms.


**Creating and Using Generics in TypeScript 4** by by Brice Wilson (2021-05-25)
- TypeScript generics empower you to create reusable, type-safe code for your web applications. This course will teach you how to recognize and use built-in generics as well as how to create your own generic functions, interfaces, and classes.




#### 2020


**PUG Template Engine Master Course** by Joe Santos Garcia (June 07, 2020)
- Make HTML Fun Again! Node JS Template Engine - simple quick Pug Cours


**HTML and CSS: Creating a Basic Website** by Jon Friskics (June 06, 2020)
- Learn the fundamentals of HTML and CSS, while building a strong foundation for more advanced front-end development. This interactive course was formerly known as Front-end Foundations on Code School.


**HTML Fundamentals** by Matt Milner (June 02, 2020)
- Learn the primary building block of web development: HTML. Get a firm understanding of how HTML markup becomes web pages with images, text, and lists.


**SinonJS Fundamentals** by Nate Taylor (Mar 08, 2020)
- SinonJS is one of the most popular JavaScript libraries for test doubles. This course walks through the various features of SinonJS so that you know not only how to use each feature, but more importantly, why you would use each one.


**RESTful Web Services with Node.js and Express** by Jonathan Mills (Mar 01, 2020)
- Node.js is a simple and powerful tool for backend development. When combined with Express, you can create lightweight, fast, scalable APIs quickly and simply. With REST, those APIs become simple and user-friendly to make your APIs more usable.

**AWS Network Design: Getting Started** by Craig Golightly (Feb 25, 2020)
- Get started with an overview of AWS networking in the cloud so you can take advantage of services to make building and managing your application easier. This course covers what you need to know about cloud networking in an easy-to-understand format.

**Creating Asynchronous TypeScript Code** by John Papa (Feb 10, 2020)
- This course gently teaches you how to identify when to write asynchronous code, how to pass callback functions as parameters to functions, how to create and resolve/reject promises, and create and handle asynchronous functions with async/await.

**Using Specialized Types and Language Features in TypeScript** by Hendrik Swanepoel (Feb 06, 2020)
- Ever stumbled across a TypeScript issue and became totally overwhelmed with the abstract documentation of advanced concepts? This course will help you solve even the most challenging typing and modeling issues that you will encounter in TypeScript.

**Creating and Using TypeScript Decorators** by David Tucker (Feb 06, 2020)
- This course will cover how to leverage all types of TypeScript decorators to implement cross-cutting concerns within your projects.

**Creating and Using Generics in TypeScript** by Brice Wilson (Feb 06, 2020)
- TypeScript generics empower you to create reusable, type-safe code for your web applications. This course will teach you how to recognize and use built-in generics as well as how to create your own generic functions, interfaces, and classes.

**Creating Object-oriented TypeScript Code** by Dan Wahlin (Feb 06, 2020)
- This course dives into object-oriented TypeScript code, teaching you about object-oriented principles and how they can be applied to your TypeScript applications. Learn about objects, classes, inheritance, abstract classes, interfaces, and more.

**TypeScript: Getting Started** by Brice Wilson (Feb 05, 2020)
- TypeScript is a powerful, fun, and popular programming language used for building browser and NodeJS applications. This course will teach you all of the most important features of TypeScript, and quickly make you productive with the language.

**Node.js: Getting Started** by Samer Buna (Jan 31, 2020)
- The Node.js runtime powers back-end servers for big players like PayPal, Netflix, LinkedIn, and even NASA. This course will teach you the fundamentals of this very popular runtime and get you comfortable writing code for Node.

**JavaScript Best Practices** by Jonathan Mills (Jan 16, 2020)
- Identify and prevent common problems and headaches in JavaScript by learning best practices. From syntax oddities, to async patterns, to callbacks, this course will help you walk through how to deal with some of JavaScript's problem spots.

**JavaScript Objects and Prototypes** by Jim Cooper (Jan 14, 2020)
- This course teaches the in-depth, behind-the-scenes details of creating JavaScript objects, manipulating properties, and using prototypal inheritance.

**JavaScript: Getting Started** by Mark Zamoyta (Jan 14, 2020)
- JavaScript is the popular programming language which powers web pages and web applications. If you are new to programming or just new to the language, this course will get you started coding in JavaScript.


**Getting Started with Docker Swarm Mode** by Wes Higbee (11.01.2020)
- What if you could throw containers at a cluster of nodes as easily as a single node? This course will teach how you can control a cluster with all the simplicity of managing a single docker engine thanks to Docker Swarm Mode.

**NoSQL: The Big Picture** by Andrew Brust (Jan 12, 2020)
- A comprehensive presentation covering NoSQL technology, its business impact and guidelines for its adoption.

**TypeScript: The Big Picture** by Simon Allardice (Jan 11, 2020)
- This course is a technical introduction and overview of TypeScript — a language that “wraps around” JavaScript to help us avoid common issues and improves not just the code itself, but the entire developer experience writing JavaScript apps.

**Node.js: The Big Picture** by Paul O'Fallon (Jan 11, 2020)
- Curious if Node.js is right for you? In this course we look at how Node is commonly used, examine its asynchronous development model, explore leveraging the npm module ecosystem, and assemble a set of tools to help get you started on your journey!


**Managing Docker Images** by by David Clinton (07.01.2020)
- Looking to effectively and securely build and administrate Docker images? This course will introduce you to the tools, with a particular focus on learning to host your own private image repository using the open source Docker Registry.


**Getting Started with OAuth 2.0** by Scott Brady (04.01.2020)
- OAuth 2.0 is the go-to solution for API security, bringing authorization and delegation to modern HTTP APIs. In this course, you'll learn the fundamentals of OAuth, allowing you to architect and implement the right solution for your requirements.


#### 2019

**Getting Started with Docker on Windows** by Wes Higbee (29.12.2019)
-Would you like to know how to simplify running software? This course will cover running both Linux and Windows software in Windows environments through the use of containerization.

**Testing with EF Core** by Kevin Dockx (23.12.2019)
- When creating an application that uses EF Core you’ll quickly wonder how you can test your code. In this course you’ll learn about strategies that can help with testing your code by using in-memory database providers like InMemory and SQLite.

**Working with Nulls in C#** by Jason Roberts (22.12.2019)
- Learn how to create, check, access, and eliminate nulls in your C# applications.


**Getting Started with Docker** by Nigel Poulton (14.12.2019)
- This course will get learners quickly up to speed with the fundamentals of Docker and containers. The course includes major new features introduced in Docker 1.12, including Swarm mode, services, and stacks.

**Docker and Kubernetes: The Big Picture** by Nigel Poulton (12.12.2019)
- Docker and Kubernetes are transforming the application landscape - and for good reason. This course is the perfect way to get yourself – and your teams – up to speed and ready to take your first steps.

**Designing RESTful Web APIs** by Shawn Wildermuth (12.12.2019)
- Are you embarking on creating an API for your website or mobile app? If so, just striking forward with your API could be a mistake. In this course, you will learn how to design an API to meet the demands of your customers.

**Effective Logging in ASP.NET Core** by Erik Dahl (Dec 01, 2019)
- In this course you will learn how to create great log entries and then get them written to places that will make them easy to use. You will learn all of the techniques you will need to make your apps easily supportable via great logging. 

**C#: Using LINQ Queries & Operators** by Eric Fisher (Nov 30, 2019)
- This course introduces the basics of LINQ and its most common operators. You'll learn to use Select, Where, OrderBy, and GroupBy to structure, filter, and organize data in C# using LINQ.

**JavaScript Fundamentals** by Mark Zamoyta (Nov 30, 2019)
- Learn everything you need to know to produce production quality web applications and web page features with this foundational course on JavaScript.


**Dependency Injection in ASP.NET Core** by Steve Gordon (Nov 23, 2019)
- This course will teach you everything you need to know about using dependency injection in ASP.NET Core. The skills you will learn will help you to build complex ASP.NET Core applications that make full use of dependency injection.

**Using Configuration and Options in .NET Core and ASP.NET Core Apps** by Steve Gordon (Nov 23, 2019)
 - This course will teach you everything you need to know about using configuration and options in ASP.NET Core. The skills you will learn will help you to build complex ASP.NET Core applications which can be configured from multiple sources.

**More Effective LINQ** by Mark Heath (Nov 23, 2019)
 - Learn how to fully harness the power of LINQ by exploring best practices and avoiding common pitfalls by solving some fun and challenging problems.

**Introduction to CSS** by Scott Allen (Nov 07, 2019)
 - This course introduces you to Cascading Style Sheets (CSS)

**Building Web Applications with ASP.NET Core MVC** by Gill Cleeren (Nov 01, 2019)
- In this course, you'll learn how to build a complete application with ASP.NET Core 3.0 MVC using Visual Studio 2019 in a practical, hands-on way.

**Continuous Delivery and DevOps with Azure DevOps: Managing Builds** by Marcel de Vries (Nov 01, 2019)
- In this course, you will learn how to set up an automated build with Azure DevOps or Azure DevOps Server pipelines. By the end of this course, you will be able to implement continuous integration builds that are the fundament for your DevOps teams.

**Building an API with ASP.NET Core** by Shawn Wildermuth (Oct 30, 2019)
- The skill of building a web-facing API isn’t optional anymore. Whether you’re building a web site, mobile app, SPA, or enterprise tool, building a well-designed API is required.

**ASP.NET Core Fundamentals** by Scott Allen (Oct 30, 2019)
 - This course shows you all the features you'll need to build your first application with ASP.NET Core. 

**C# Events, Delegates and Lambdas** by Dan Wahlin (Nov 12, 2019)
- This course provides an in-depth look at how events, delegates and lambdas work in the C# language and .NET framework.

**JavaScript: Getting Started** by Mark Zamoyta (Oct 20, 2019)
- JavaScript is the popular programming language which powers web pages and web applications. If you are new to programming or just new to the language, this course will get you started coding in JavaScript. 

**Getting Started with Entity Framework 6** by Julie Lerman (Nov 02, 2019)
- This course will help you understand the goals and benefits of using Entity Framework 6 for data access and guide you through creating your first code-based model to store and retrieve data using EF’s APIs.

**React: The Big Picture** by Cory House (Oct 18, 2019)
- Interested in React? This course explores why React is worth considering, tradeoffs to consider, and reasons React may, or may not be the right fit for you.

**HTML, CSS, and JavaScript: The Big Picture** by Barry Luijbregts (Nov 04, 2019)
- In this course, you'll learn what HTML, CSS, and JavaScript are and how they interact to bring content to your browser. 

**LINQ Fundamentals** by Scott Allen (Sep 14, 2019)
- Big changes have been made to C# thanks to LINQ. This course will give you everything you need to work with the Language Integrated Query (LINQ) features of C#, using practical examples and demonstrating some best practices.

**Dates and Times in .NET** by Filip Ekberg (Aug 27, 2019)
- Learn the fundamentals and master the art of working with dates and times in .NET; a topic commonly taken for granted, but often the cause of unexpected errors. This course will make you comfortable working with dates and times in any .NET project!



### [Udacity Courses](https://www.udacity.com/):
- **[AI Programming with Python Nanodegree Program](https://www.udacity.com/course/ai-programming-python-nanodegree--nd089)** (01.2019)
    - TIMELINE - One 3-month term - Study 10 hrs/week

- [Intro to JavaScript - Fundamentals of the JavaScript Syntax](https://www.udacity.com/course/intro-to-javascript--ud803) (24.06.2018)
    - TIMELINE Approx. 2 weeks 
- [Intro to HTML and CSS - Build styled, well-structured websites](https://www.udacity.com/course/intro-to-html-and-css--ud001) (26.06.2018)
    - TIMELINE Approx. 3 weeks 
- [Networking for Web Developers - TCP, IP, What's Underneath HTTP?](https://www.udacity.com/course/networking-for-web-developers--ud256) (27.06.2018)
    - TIMELINE Approx. 2 weeks 
- [Version Control with Git - Learn to track and manage changes](https://www.udacity.com/course/version-control-with-git--ud123) (28.06.2018)
    - TIMELINE Approx. 4 weeks

### [EDX Courses](https://edx.org):

- **[DEV204.2x: Object Oriented Programming in C#](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/Microsoft_DEV204.2x_Certificate___edX.pdf)** (13.04.2019)
    - Duration (3–5 hours per week, for 3 weeks)

- **[DEV204.1x: Introduction to C#](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/Microsoft_DEV204.1x_Certificate___edX.pdf)** (08.04.2019)
    - Duration (3–5 hours per week, for 3 weeks)
    
- **[DEV210.3x: Advanced C++](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/Microsoft_DEV210.3x_Certificate___advanced_cpp_edX.pdf)** (07.02.2019)
    - Duration (2-4 hours per week, for 4 weeks)


- **[DEV279x: Building Interactive Prototypes using JavaScript](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/Microsoft_DEV279x_Certificate___edX.pdf)** (19.12.2018)

### [Udemy Courses](https://www.udemy.com/):

- **[The Complete C# Developers: Code the Right Way](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/UC-777PRPV8.pdf)**(18.08.2019)

- **[C# Intermediate: Classes, Interfaces and OOP](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/UC-UAX000B5.pdf)** (21.05.2019)

-  **[C# Basics for Beginners: Learn C# Fundamentals by Coding](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/UC-DSTIUMGR.pdf)** (01.05.2019)

- **[Complete Python Bootcamp](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/UC-MG1XT0OI.pdf)** (29.01.2019)


### [ProgramUtvikling Courses](https://programutvikling.no/):

- **[Building modern applications with C# and .NET Core](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/building_modern_applicatons_with_cs_and_dotnet_core.pdf)** (21.11.2019)  -  Duration (4 days)


- **[Agile Object-Oriented Design](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/agile_oop_design.pdf)** (04.11.2016)

- **[Test Driven Development](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/tdd_certificate_programmutvikling.pdf)** (22.04.2016)

- **[Python Programming](https://gitlab.com/brianopedal/portfolio/blob/master/course_certificates/python_programming_course.pdf)** (16.03.2016)

- **[Advanced C++ and Intro to C++11](https://gitlab.com/-/ide/project/brianopedal/portfolio/blob/master/-/course_certificates/advanced_cpp_and_intro_to_cpp11.pdf)** (17.09.2015)


### [Khan Academy Courses](https://www.khanacademy.org/)


Khanacademy - Linear Algebra
- Vectors and spaces
- Matrix transformations
- Alternate coordinate systems (bases)

### Other: Courses, Events & Workshops
- **[WebValley: Learn JavaScript Fundamentals (ES6)](https://www.meetup.com/WebValley-Kongsberg-meetup/events/259876582/)** (23.03.2019)


### Books - Completed Reading:

- **[SQL Primer - An Accelerated Introduction to SQL Basics](https://www.apress.com/gp/book/9781484235751)** *by Rahul Batra* (19.04.2019)
    - Decent well explained intro with examples

- **[Grokking algorithms: An illustrated guide for programmers and other curious people](https://www.manning.com/books/grokking-algorithms)** *by Aditya Y. Bhargava* (24.03.2019)
    - Good book - algorithms and data structures explained through clean simple drawings
    - More books should go this route - aimed at visual learners


- **[Clean Architeture](https://www.goodreads.com/book/show/18043011-clean-architecture)** *by Robert C. Martin* (17.03.2019)
    - Decent book (Requires, second or third reading - workthrough to fully understand)
    - Mentions several other interesting design books


- **[Git for Humans](https://abookapart.com/products/git-for-humans)** *by David Demaree*  (11.10.2018)
    - Good book, with clear Git use explanations.
    - Resource page holds a lot of interesting Git learning references.




    





